# Manage a nginx vhost that reverse proxies to a LimeSurvey instance.
class tails::limesurvey::reverse_proxy (
  Stdlib::Fqdn $public_hostname     = 'survey.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname   = 'survey.lizard',
  Stdlib::Port $upstream_port       = 80,
  ) {

  $nginx_includename = 'tails_limesurvey_reverse_proxy'
  $upstream_address = "${upstream_hostname}:${upstream_port}"

  nginx::vhostsd { $public_hostname:
    content => template('tails/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included[$nginx_includename],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { $nginx_includename:
    content => template('tails/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
