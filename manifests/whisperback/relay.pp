# Manage a SMTP relay used by WhisperBack to send email to Tails help desk
class tails::whisperback::relay (
  Optional[String] $always_bcc = undef,
) {

  $stretch_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  package { 'swaks': ensure => installed }

  augeas { 'loopback_interface' :
    context => '/files/etc/network/interfaces',
    changes =>
    [
      "set auto[child::1 = 'lo']/1 lo",
      "set iface[. = 'lo'] lo",
      "set iface[. = 'lo']/family inet",
      "set iface[. = 'lo']/method loopback",
      "set iface[. = 'lo']/up 'ip addr add 127.0.0.2/32 dev lo'",
    ],
  }

  $onion_dir = '/var/lib/tor/tails_whisperback_relay'
  $postfix_instance_conf_dir = '/etc/postfix-hidden'

  apt::pin { 'tor':
    ensure     => $stretch_and_older_only_ensure,
    packages   => ['tor', 'tor-geoipdb'],
    originator => 'Debian Backports',
    codename   => 'stretch-backports',
    priority   => 991,
  }

  include tor::daemon

  file { $onion_dir:
    ensure  => directory,
    recurse => true,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => Package['tor'],
    notify  => Service['tor'],
    source  => 'puppet:///modules/tails_secrets_whisperback/tor',
  }

  tor::daemon::snippet { 'tails_whisperback_relay':
    content => "# Onion service tails_whisperback_relay
HiddenServiceDir /var/lib/tor/tails_whisperback_relay
HiddenServicePort 25 127.0.0.2:25
HiddenServiceVersion 3

",
    require => [
      File[$onion_dir],
      Service['postfix@postfix-hidden'],
    ],
  }

  postfix::config {
    'multi_instance_wrapper':
      # lint:ignore:single_quote_string_with_variables -- Postfix variable
      value => '${command_directory}/postmulti -p --}';
      # lint:endignore
    'multi_instance_enable':
      value => 'yes';
    'multi_instance_directories':
      value   => $postfix_instance_conf_dir,
      require => File[$postfix_instance_conf_dir];
  }

  service { 'postfix@postfix-hidden':
    ensure    => running,
    enable    => true,
    subscribe => Postfix::Hash['/etc/postfix/tls_policy'],
    require   => [
      Service['postfix'],
      Postfix::Config['multi_instance_directories'],
      File["${postfix_instance_conf_dir}/dynamicmaps.cf"],
    ],
  }

  file { $postfix_instance_conf_dir:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  postfix::hash { "${postfix_instance_conf_dir}/access":
    source  => 'puppet:///modules/tails/whisperback/relay/access',
    require => File[$postfix_instance_conf_dir],
  }

  file { "${postfix_instance_conf_dir}/aliases":
    content => template('tails/whisperback/relay/aliases.erb'),
    require => File[$postfix_instance_conf_dir],
    notify  => Exec["generate ${postfix_instance_conf_dir}/aliases.db"],
  }
  file { "${postfix_instance_conf_dir}/aliases.db":
    ensure  => present,
    require => [
      File["${postfix_instance_conf_dir}/aliases"],
      Exec["generate ${postfix_instance_conf_dir}/aliases.db"]
    ],
  }
  exec { "generate ${postfix_instance_conf_dir}/aliases.db":
    command     => "postalias ${postfix_instance_conf_dir}/aliases",
    refreshonly => true,
    require     => Package['postfix'],
    subscribe   => File["${postfix_instance_conf_dir}/aliases"],
  }

  file {
    "${postfix_instance_conf_dir}/master.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/tails/whisperback/relay/master.cf',
      notify  => Service['postfix@postfix-hidden'],
      require => Package['postfix'];

    "${postfix_instance_conf_dir}/main.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('tails/whisperback/relay/main.cf.erb'),
      notify  => Service['postfix@postfix-hidden'],
      require => [
        Package['postfix'],
        File['/var/lib/postfix-hidden'],
        File['/var/spool/postfix-hidden'],
        File["${postfix_instance_conf_dir}/access.db"],
        File["${postfix_instance_conf_dir}/aliases.db"],
      ];
    '/var/lib/postfix-hidden':
      ensure => directory,
      owner  => 'postfix',
      group  => 'postfix',
      mode   => '0755';
    '/var/spool/postfix-hidden':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    "${postfix_instance_conf_dir}/dynamicmaps.cf":
      ensure => link,
      target => '/etc/postfix/dynamicmaps.cf',
      notify => Service['postfix@postfix-hidden'];
  }

  # XXX: is this still needed, now that instanciated postfix@.service
  # has ExecStartPre=/usr/lib/postfix/configure-instance.sh?
  exec { 'copy_postfix_chroot':
    command => 'cp -a /var/spool/postfix/etc /var/spool/postfix-hidden/etc',
    onlyif  => 'test ! -d /var/spool/postfix-hidden/etc',
    require => File['/var/spool/postfix-hidden'],
  }

  @@::tails::monitoring::service::postfix_mailqueue { "hidden_mailqueue@${::tails::monitoring::config::nodename}":
    nodename   => $::tails::monitoring::config::nodename,
    zone       => $::tails::monitoring::config::zone,
    tag        => $::tails::monitoring::config::parent_zone,
    config_dir => $postfix_instance_conf_dir,
  }

}
