class tails::monitoring::checkcommand::postfix_mailqueue (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_postfix_mailqueue

  file { '/etc/icinga2/conf.d/check_postfix_mailqueue.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_postfix_mailqueue.conf',
    require => Class['tails::monitoring::plugin::check_postfix_mailqueue'],
    notify  => Service['icinga2'],
  }

}
