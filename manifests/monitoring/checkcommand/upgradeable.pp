class tails::monitoring::checkcommand::upgradeable (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_upgradeable

  file { '/etc/icinga2/conf.d/check_upgradeable.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_upgradeable.conf',
    require => Class['tails::monitoring::plugin::check_upgradeable'],
    notify  => Service['icinga2'],
  }

}
