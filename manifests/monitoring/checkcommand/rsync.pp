class tails::monitoring::checkcommand::rsync (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_rsync

  file { '/etc/icinga2/conf.d/check_rsync.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_rsync.conf',
    require => Class['tails::monitoring::plugin::check_rsync'],
    notify  => Service['icinga2'],
  }

}
