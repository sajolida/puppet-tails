class tails::monitoring::checkcommand::torbrowser_archive (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_torbrowser_archive

  file { '/etc/icinga2/conf.d/check_torbrowser_archive.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_torbrowser_archive.conf',
    require => Class['tails::monitoring::plugin::check_torbrowser_archive'],
    notify  => Service['icinga2'],
  }

}
