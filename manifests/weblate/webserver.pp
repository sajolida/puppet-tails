# Setup the webserver to serve the translation platform
class tails::weblate::webserver(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $apache_data_dir,
  Stdlib::Absolutepath $code_git_checkout,
) {

  ensure_packages([
    'apache2',
    'modsecurity-crs',
    'libapache2-mod-security2',
    'libapache2-mod-wsgi-py3',
  ])

  service { 'apache2':
    ensure  => running,
    require => Package[apache2],
  }

  exec { 'a2dismod rewrite':
    onlyif  => 'test -f /etc/apache2/mods-enabled/rewrite.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }


  # static data served by Apache without going through wsgi

  file { $apache_data_dir:
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2755',
  }

  file { "${apache_data_dir}/media":
    ensure  => directory,
    owner   => root,
    group   => weblate,
    mode    => '2775',
    require => File[$apache_data_dir],
  }

  file { "${apache_data_dir}/static":
    ensure  => directory,
    owner   => root,
    group   => weblate,
    mode    => '2775',
    require => File[$apache_data_dir],
  }

  file { "${mutable_data_dir}/config/apache-vhost.conf":
    ensure  => present,
    content => template('tails/weblate/apache-vhost.erb'),
    owner   => root,
    group   => 'weblate_admin',
    mode    => '0664',
    notify  => Service[apache2],
    require => File[$mutable_data_dir],
  }

  file { '/etc/apache2/sites-available/000-default.conf':
    ensure  => symlink,
    target  => "${mutable_data_dir}/config/apache-vhost.conf",
    require => Package[apache2],
    notify  => Service[apache2],
  }

  file { '/etc/modsecurity/modsecurity.conf':
    ensure  => present,
    source  => 'puppet:///modules/tails/weblate/modsecurity/modsecurity.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package[modsecurity-crs],
    notify  => Service[apache2],
  }

  file { '/etc/modsecurity/crs/crs-setup.conf':
    ensure  => present,
    source  => 'puppet:///modules/tails/weblate/modsecurity/crs-setup.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package[modsecurity-crs],
    notify  => Service[apache2],
  }

}
