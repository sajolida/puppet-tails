# Manages the Tails Jenkins master.
# Installs extra packages for nice features as well as Jenkins plugins.
# If $automatic_iso_jobs_generator is 'present', $jenkins_jobs_repo must be
# set to the URL of a jenkins-jobs git repo where the 'jenkins@jenkins-master'
# SshKey has write access. It depends on $deploy_on_git_push being set to true
# for the pushed configuration to be applied automatically.
# For this to happen, hooks managed in tails::gitolite::hooks::jenkins_jobs
# also need to be installed in the jenkins-jobs repo.

class tails::jenkins::master (
  String $jenkins_jobs_repo,
  String $version                                         = '2.176.3',

  String $tails_repo                                      = 'https://gitlab.tails.boum.org/tails/tails.git',
  Boolean $deploy_jobs_on_git_push                        = true,
  Enum['present', 'absent'] $automatic_iso_jobs_generator = 'present',
  Integer $active_branches_max_age_in_days                = 49,
  String $gitolite_pubkey_name                            = 'gitolite@puppet-git',

  Boolean $manage_mount                                   = false,
  $mount_device                                           = false,
  $mount_fstype                                           = 'ext4',
  $mount_options                                          = 'relatime,user_xattr,acl',

  String $monitoring_parent_zone                          = 'Lizard',
) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $mount_point = '/var/lib/jenkins'
  $ssh_pubkey_name = "jenkins@jenkins-master.${::domain}"

  ### Resources

  apt::pin { 'jenkins':
    packages => 'jenkins',
    version  => $version,
    priority => 991,
  }

  class { 'jenkins':
    repo            => true,
    lts             => true,
    install_java    => false,
    version         => $version,
    default_plugins => [],
    require         => [
      Package[$base_packages],
      Apt::Conf['proxy_jenkins_repo'],
      Apt::Pin['jenkins'],
    ],
  }

  # apt-cacher-ng does not support HTTPS repositories
  apt::conf { 'proxy_jenkins_repo':
    content  => 'Acquire::HTTP::Proxy::pkg.jenkins.io "DIRECT";',
  }
  apt::conf { 'proxy_prodjenkinsreleases_repo':
    content  => 'Acquire::HTTP::Proxy::prodjenkinsreleases.blob.core.windows.net "DIRECT";',
  }

  apt::pin { 'jenkins-job-builder':
    packages   => [
      'jenkins-job-builder',
      'python3-jenkins-job-builder',
      'python3-jenkins'
    ],
    originator => 'Debian Backports',
    priority   => 991,
  }

  $base_packages = [
    'git',
    'jenkins-job-builder',
    'libmockito-java',
    'python3-jenkins-job-builder',
    'python-pkg-resources',
  ]

  ensure_packages($base_packages)

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)
    validate_string($monitoring_parent_zone)

    # Needs to be created by hand before applying this, if $manage_mount
    # is true. We cannot manage File[$mount_point] ourselves as this
    # would duplicate the same declaration in jenkins::config.
    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

    Mount[$mount_point] -> Class['jenkins']

    @@::tails::monitoring::service::disk { "jenkins-data-disk@${::fqdn}":
      nodename  => $::fqdn,
      zone      => $::fqdn,
      partition => $mount_point,
      wfree     => '30000',
      cfree     => '20000',
      tag       => $monitoring_parent_zone,
    }
  }

  include nfs::server

  Nfs::Export <<| tag == $::fqdn |>>

  # lint:ignore:140chars -- SHA512

  jenkins::plugin { 'apache-httpcomponents-client-4-api':
    version       => '4.5.10-1.0',
    digest_string => '9136a225a0006d5a812126360163558e486197a0db5ca556eac1032b14d874db',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'build-timeout':
    version       => '1.19',
    digest_string => 'a92b43adb9c668e3fd0ad307db43c2277cf15ea75c084b5bdc74fb294f80583d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'cloudbees-folder':
    version       => '6.9',
    digest_string => '6a7e2b24b48438dee4b8bf25f615059cb8f32f5ad228b16f2e1090e670956d55',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'f09b82ccec2afc60c3b9235d804f312c3e5a0a847b4243b1e3546f718f344af1f7a0f26c4d53a02ae360aa6a50a20676910b143011c4eb880daa8ab0bc0fb073',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'conditional-buildstep':
    version       => '1.3.6',
    digest_string => '4b550bc136fe66bb4eb396605f4036935963327b9c94662f7c441888adb99f77',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'maven-plugin',
      'run-condition',
      'token-macro',
    ],
  }

  jenkins::plugin { 'copyartifact':
    version       => '1.42.1',
    digest_string => 'd7f26e2c17114850668d36c1db0df877c7a26ede519763234f8d5f8dca5de89c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials':
    version       => '2.3.0',
    digest_string => 'd781da60f2d2bba575881ad38be08dd226222608601abe8a6c92e394c36e129c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'cucumber-reports':
    version       => '4.10.0',
    digest_string => '1218efe4e476019506bb63e812e8ff88dcdd9bcf11b2caac1d3be56bfff929fe',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'cucumber-testresult-plugin':
    version       => '0.10.1',
    digest_string => '2d2f171a8561ec91a11def39a3f1e75302516f097e86157f5bdd1402f29858bf',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'display-url-api':
    version       => '2.3.2',
    digest_string => 'a4d5f37349930b5dd9a2a5042bd13527b4c2ee316fe49420fe154fa0623a2bb1',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'downstream-ext':
    version       => '1.8',
    digest_string => '5033490d9b34943488e387d64a3a09cf02a43dced29b0f43e8a68b9d837a1869702f9080831fc80e64e2addebf0553bd5c6a8793b46182ed1535422ca839d27f',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'durable-task':
    version       => '1.30',
    digest_string => '580c22a7218b0f3fcd9a0705bba4b919e08d3e23db478342634d8ed8122bc7ed',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'email-ext':
    version       => '2.66',
    digest_string => '65cac44728f454f89eb60c015a239a9eb765108a8c620e3be935bb22e4e60305',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'mailer',
      'matrix-project',
      'script-security',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '2.3.0',
    digest_string => '161010a696fbe1d74a5e2f846794d79e094fae17e8cfa5ffc78810b822cfbc6e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'envinject-api',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'envinject-api':
    version       => '1.7',
    digest_string => '0fb5c4d0b0fbc112addedd604a94d94ce5c42dde3cc6b2494bbf706cdb249a9b',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'git':
    version       => '3.12.1',
    digest_string => '6e02bf67623c4a4e01bbf01f73af21714ffad4068f705856f69118c4af6bb1ea',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'git-client',
      'mailer',
      'matrix-project',
      'scm-api',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '2.8.6',
    digest_string => '3a2bfd7aeb2ebb13b012824ddffa93d06814c9011e3525f056dbe1e917830860',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'credentials',
      'jsch',
      'ssh-credentials',
      'structs',
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '1.5',
    digest_string => '36b1aeecd6f6cd96263baca7143e4e201f93cc4797b814e62527cf58a9fd4b82',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',
    ],
  }

  jenkins::plugin { 'icon-shim':
    version       => '2.0.3',
    digest_string => 'a83ebce40c28b4bb2474eea3aecfea9c079904b02dee210a70d5ffd7455c437e50ad59b1a9c677a749f9eae84ac702e5d9726a68b63d64ae5c209f5d105418b3',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'javadoc':
    version       => '1.5',
    digest_string => '25514fb702740cbb883fc5c6eeb86177b118517f9b0157f1b2e0c60e8bef1564',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jsch':
    version       => '0.1.55.1',
    digest_string => '934151600dd42b0398e881fbba70656c38fd6a6f12a874d23fc8dc35e371c4d4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['ssh-credentials'],
  }

  jenkins::plugin { 'junit':
    version       => '1.28',
    digest_string => 'a471c80776b9684c4ee9164ce51e01b9871af664bdfce13b11320020ddc25f33',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'script-security',
      'structs',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'mailer':
    version       => '1.28',
    digest_string => 'fe13df084a337522411c70bba1cb2e9c63bcdab322b77e559dc19fc08fde5a09',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['display-url-api'],
  }

  jenkins::plugin { 'mapdb-api':
    version       => '1.0.9.0',
    digest_string => '072c11a34cf21f87f9c44bf01b430c5ea77e8096d077e8533de654ef00f3f871',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'matrix-project':
    version       => '1.14',
    digest_string => '88d84ef75ea63c3ed826caecb2bc03ed59206fd164288f02bade7ce2685a388a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'script-security',
    ],
  }

  jenkins::plugin { 'maven-plugin':
    version       => '3.4',
    digest_string => 'b554ff3395232ddc78f8bf6dd150e8a0994c32a01e16b661d257c6f95d7b44c3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'javadoc',
      'jsch',
      'junit',
      'mailer',
    ],
  }

  jenkins::plugin { 'parameterized-trigger':
    version       => '2.35.2',
    digest_string => '36228ae6c41cf828cf472deabaeff50f8b4e9b69e742deab38d4a9b9a093fd97',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'conditional-buildstep',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'postbuildscript':
    version       => '2.9.0',
    digest_string => '9bd90ecf440ae9f7a2871ae929a8bf309cc078b2f529e4848bb0d0ad66286656',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['matrix-project'],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '3.6.0',
    digest_string => 'a548df16d9a1744c4a5cd2d27c9ed718d14670c346feb916218e2a4960612043',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'resource-disposer':
    version       => '0.13',
    digest_string => 'f0820b7260b7a22aa0c461d8b76cca2140bc15cbe0003650e4821dc99095bb44',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'run-condition':
    version       => '1.2',
    digest_string => '1dbfae6b57c4ae0e190354ac273280bba135aaba82c2d8116bd394c4b83d5e5f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'token-macro',
    ],
  }

  jenkins::plugin { 'scm-api':
    version       => '2.6.3',
    digest_string => '83262d406862ad55ff90d36020a83f88ebd71e66f49c75c9a5140f43176aba29',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'scm-sync-configuration':
    version       => '0.0.10',
    digest_string => '84606ed21b72918a5633cf8e438116ab9af3d9010b2651336af92ce8474e0870',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['subversion'],
  }

  jenkins::plugin { 'script-security':
    version       => '1.64',
    digest_string => '3e7698ed76e5e25c881d7380fb160e590eb535ad23a94c66eefd51087bca4a84',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'simple-theme-plugin':
    version       => '0.5.1',
    digest_string => 'd823ac7fa1d5861051fc69534f5678c32d10c98a00987a045b0b7836fd733584',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '1.17.2',
    digest_string => '069d52ebc301d51cc74331457c2d95aed4404627f40770a951477369c0df60a5',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['credentials'],
  }

  jenkins::plugin { 'structs':
    version       => '1.20',
    digest_string => '7e7861356a37aa6a727462d7aea716dd9307071252f7349c2726d64a773feb3a',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'subversion':
    version       => '2.12.2',
    digest_string => 'fbf03d81544fc78e614ef0a212d66ce91c856041e53d49748a82f1ae8d6d53ca',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'mapdb-api',
      'scm-api',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
    ],
  }

  jenkins::plugin { 'timestamper':
    version       => '1.10',
    digest_string => '1dbbea462c5723462b6b3081aa984a682e06e3e06ce2c23570b44faf76faf814',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'token-macro':
    version       => '2.8',
    digest_string => '5b068a58d8bbc91a74cffd137eaa19602d7d8d5ce9018cc00ad67e33b7c3283c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'workflow-api':
    version       => '2.37',
    digest_string => '9cb845dec483f7a1954c19992524948e8b9b31462489fb208b43be054dd9b821',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-durable-task-step':
    version       => '2.34',
    digest_string => 'd1a91f9c175e0f1cf17afdf63c264ab8e66e4370ef1d9822243404091d9dc3bf',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'durable-task',
      'scm-api',
      'script-security',
      'structs',
      'workflow-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'workflow-step-api':
    version       => '2.20',
    digest_string => '6dff04dc0fb38e123985412ad1a72a45f9ee199cbb847c116bf6b7cc5edce7aa',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'workflow-support':
    version       => '3.3',
    digest_string => '0f2b18d0de9b7c94abc03701e33f660620382dd4fcc83600d6aafcb5888d8f51',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-scm-step':
    version       => '2.9',
    digest_string => '63b105ed776e269f6915052218d5cd731da60cabe2291bbfd0718ea4e384b981',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['workflow-step-api'],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.37',
    digest_string => '9d74adcc911e1b08c7412c23aa55f6aa6d016f000587d255598ce16754d8a90d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'resource-disposer',
      'workflow-durable-task-step',
    ],
  }
  # lint:endignore

  ## Uncomment this (or similar) once all this is moved to a proper class,
  ## that inherits jenkins::service and can thus append to its dependencies.
  # Service['jenkins'] {
  #   require +> File_line['jenkins_HTTP_HOST'],
  # }

  file { '/etc/jenkins_jobs/jenkins_jobs.ini':
    owner   => root,
    group   => jenkins,
    mode    => '0640',
    source  => 'puppet:///modules/tails/jenkins/master/jenkins_jobs.ini',
    require => [
      Package['jenkins'],
      Package['jenkins-job-builder'],
    ],
  }

  file { '/etc/jenkins_jobs':
    ensure => directory,
    owner  => root,
    group  => jenkins,
    mode   => '0770',
  }

  vcsrepo { '/etc/jenkins_jobs/jobs':
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => jenkins,
    provider => git,
    source   => $jenkins_jobs_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
      Package['git'],
      File['/etc/jenkins_jobs'],
    ],
  }

  if $deploy_jobs_on_git_push {
    file { '/var/tmp/jenkins_jobs_test':
      ensure => directory,
      owner  => jenkins,
      group  => jenkins,
      mode   => '0700',
    }

    file { '/usr/local/sbin/deploy_jenkins_jobs':
      ensure  => present,
      source  => 'puppet:///modules/tails/jenkins/master/deploy_jenkins_jobs',
      owner   => root,
      group   => root,
      mode    => '0755',
      require => [
        File['/var/tmp/jenkins_jobs_test'],
        Ssh_authorized_key[$gitolite_pubkey_name],
      ],
    }

    sshkeys::set_authorized_keys { $gitolite_pubkey_name:
      user    => jenkins,
      home    => '/var/lib/jenkins',
      require => Package['jenkins'],
    }
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts':
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts',
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts_wrapper':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts_wrapper',
    require => File['/usr/local/bin/clean_old_jenkins_artifacts'],
  }

  cron { 'clean_old_jenkins_artifacts':
    command => '/usr/local/bin/clean_old_jenkins_artifacts_wrapper /var/lib/jenkins',
    user    => 'jenkins',
    hour    => '23',
    minute  => '50',
    require => [File['/usr/local/bin/clean_old_jenkins_artifacts_wrapper'],
                Package['jenkins']],
  }

  file { '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/deduplicate_reproducible_build_jobs_upstream_ISOs',
    require => Package['jenkins'],
  }

  cron { 'deduplicate_reproducible_build_jobs_upstream_ISOs':
    command => '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/6',
    require => File['/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs'],
  }

  file { '/usr/local/bin/manage_latest_iso_symlinks':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/manage_latest_iso_symlinks',
    require => Package['jenkins'],
  }

  cron { 'manage_latest_iso_symlinks':
    command => '/usr/local/bin/manage_latest_iso_symlinks /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/5',
    require => File['/usr/local/bin/manage_latest_iso_symlinks'],
  }

  class  { 'tails::jenkins::iso_jobs_generator':
    ensure            => $automatic_iso_jobs_generator,
    tails_repo        => $tails_repo,
    jenkins_jobs_repo => $jenkins_jobs_repo,
    active_days       => $active_branches_max_age_in_days,
    require           => [
      Class['jenkins'],
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
    ],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => Class['jenkins'],
  }

  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }
}
