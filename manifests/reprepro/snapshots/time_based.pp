# Manage time-based snapshots of the set of APT repositories Tails needs,
# in a reprepro setup.
#
# Parameters worth documenting:
#
#  * release_managers_sshkeys: a hash mapping each release manager name
#    to a { type => XYZ, key => XYZ } hash; each such hash will be passed
#    to ssh_authorized_key
#
# Note: the ensure parameter is not fully supported, because some resources
# we are managing here don't provide this functionality.
class tails::reprepro::snapshots::time_based (
  Hash $release_managers_sshkeys,
  Hash $architectures                          = $tails::reprepro::params::snapshots_architectures,
  Enum['present', 'absent'] $automatic_refresh = 'present',
  $email_recipient                             = 'root',
  Enum['present', 'absent'] $ensure            = 'present',
  Stdlib::Absolutepath $homedir                = '/srv/apt-snapshots/time-based',
  String $signwith                             = $tails::reprepro::params::snapshots_signwith,
  String $user                                 = 'reprepro-time-based-snapshots',
  Stdlib::Fqdn $web_hostname                   = 'time-based.snapshots.deb.tails.boum.org',
  Stdlib::Port $web_port                       = 80,
) inherits tails::reprepro::params {

  $repositories_dir = "${homedir}/repositories"

  $package_ensure = $ensure ? {
    absent  => absent,
    default => present,
  }

  tails::reprepro::snapshots::base { 'time-based':
    ensure           => $ensure,
    homedir          => $homedir,
    repositories_dir => $repositories_dir,
    user             => $user,
  }

  tails::reprepro::snapshots::time_based::repository { 'debian':
    ensure                        => $ensure,
    architectures                 => $architectures,
    automatic_refresh             => $automatic_refresh,
    automatic_refresh_delta_hours => 5,
    automatic_refresh_delta_mins  => 0,
    basedir                       => "${repositories_dir}/debian",
    homedir                       => $homedir,
    signwith                      => $signwith,
    user                          => $user,
  }

  tails::reprepro::snapshots::time_based::repository { 'debian-security':
    ensure                        => $ensure,
    architectures                 => $architectures,
    automatic_refresh             => $automatic_refresh,
    automatic_refresh_delta_hours => 5,
    automatic_refresh_delta_mins  => 15,
    basedir                       => "${repositories_dir}/debian-security",
    homedir                       => $homedir,
    signwith                      => $signwith,
    user                          => $user,
  }

  tails::reprepro::snapshots::time_based::repository { 'tails':
    ensure                        => $ensure,
    architectures                 => $architectures,
    automatic_refresh             => $automatic_refresh,
    automatic_refresh_delta_hours => 5,
    automatic_refresh_delta_mins  => 30,
    basedir                       => "${repositories_dir}/tails",
    homedir                       => $homedir,
    signwith                      => $signwith,
    user                          => $user,
  }

  tails::reprepro::snapshots::time_based::repository { 'torproject':
    ensure                        => $ensure,
    architectures                 => $architectures,
    automatic_refresh             => $automatic_refresh,
    automatic_refresh_delta_hours => 5,
    automatic_refresh_delta_mins  => 45,
    basedir                       => "${repositories_dir}/torproject",
    homedir                       => $homedir,
    signwith                      => $signwith,
    user                          => $user,
  }

  ensure_packages(['db5.3-util'], {'ensure' => $package_ensure})

  $tails_delete_expired_apt_snapshots_pkg_deps = [
    libdatetime-perl,
    libdatetime-format-mail-perl,
    libfile-find-rule-perl,
    libfile-slurp-perl,
    libipc-system-simple-perl,
    liblist-moreutils-perl,
  ]
  $tails_update_time_based_apt_snapshots_pkg_deps = [
    libcarp-assert-perl,
    libcarp-assert-more-perl,
    libipc-system-simple-perl,
    libpath-tiny-perl,
    libtry-tiny-perl,
  ]
  $tails_compact_reprepro_db_pkg_deps = [
    python3-bsddb3
  ]
  ensure_packages(
    $tails_compact_reprepro_db_pkg_deps,
    {'ensure' => $package_ensure}
  )
  ensure_packages(
    $tails_update_time_based_apt_snapshots_pkg_deps,
    {'ensure' => $package_ensure}
  )
  ensure_packages(
    $tails_delete_expired_apt_snapshots_pkg_deps,
    {'ensure' => $package_ensure}
  )

  file { '/usr/local/bin/tails-update-time-based-apt-snapshots':
    ensure  => $ensure,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/reprepro/snapshots/time_based/tails-update-time-based-apt-snapshots',
    require => [
      Package[$tails_update_time_based_apt_snapshots_pkg_deps],
      Exec[
        'tails-reprepro-snapshots-time_based-import-upstream-keys-apt-keys.d',
        'tails-reprepro-snapshots-time_based-import-upstream-keys-upstream-keys.d'
      ],
    ],
  }

  file { '/usr/local/bin/tails-bump-apt-snapshot-valid-until':
    ensure => $ensure,
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/reprepro/snapshots/time_based/tails-bump-apt-snapshot-valid-until',
  }

  file { '/usr/local/bin/tails-delete-expired-apt-snapshots':
    ensure  => $ensure,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/reprepro/snapshots/time_based/tails-delete-expired-apt-snapshots',
    require => Package[$tails_delete_expired_apt_snapshots_pkg_deps],
  }

  file { '/usr/local/bin/tails-compact-reprepro-db':
    ensure  => $ensure,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/reprepro/snapshots/time_based/tails-compact-reprepro-db',
    require => Package[$tails_compact_reprepro_db_pkg_deps],
  }

  file { '/usr/local/sbin/tails-publish-tagged-apt-snapshot':
    ensure => $ensure,
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/reprepro/snapshots/time_based/tails-publish-tagged-apt-snapshot',
  }

  sudo::conf { 'tails-publish-tagged-apt-snapshot':
    ensure  => $ensure,
    content => "${user} ALL = NOPASSWD: /usr/local/sbin/tails-publish-tagged-apt-snapshot\n",
  }

  # To avoid having to maintain them in yet another place, let's reuse
  # the keys we give to APT on our systems:
  tails::reprepro::snapshots::time_based::import_upstream_keys {
    'apt-keys.d':
      ensure  => $ensure,
      source  => 'puppet:///modules/tails/apt/keys.d',
      user    => $user,
      homedir => $homedir,
  }
  # These are the additional upstream keys we don't use elsewhere:
  tails::reprepro::snapshots::time_based::import_upstream_keys {
    'upstream-keys.d':
      ensure  => $ensure,
      source  => 'puppet:///modules/tails/reprepro/snapshots/time_based/upstream-keys.d',
      user    => $user,
      homedir => $homedir,
  }

  ensure_packages(['libnginx-mod-http-fancyindex'])

  nginx::vhostsd { $web_hostname:
    content => template('tails/reprepro/snapshots/time_based/nginx_site.erb'),
    require => Package[nginx, 'libnginx-mod-http-fancyindex'],
  }

  # Give access to our release managers, so that they can use tag-apt-snapshots
  $release_managers = keys($release_managers_sshkeys)
  tails::reprepro::snapshots::time_based::release_manager {
    $release_managers:
      sshkeys => $release_managers_sshkeys,
      user    => $user,
  }

  postfix::mailalias { $user:
    recipient => $email_recipient,
  }

  # add a cronjob to clean up the snapshot tmpdir

  file { '/etc/tmpfiles.d/time-based-apt-snapshots.conf':
    ensure  => $ensure,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => 'e  /srv/apt-snapshots/time-based/tmp/ - - - 7d'
  }
}
