# Manage Git hooks specific to tails.git
class tails::gitolite::hooks::tails () inherits tails::website::params {

  $hooks_directory = '/var/lib/gitolite3/repositories/tails.git/hooks'

  file { $hooks_directory:
    ensure => directory,
    mode   => '0755',
    owner  => gitolite3,
    group  => gitolite3,
  }

  # TODO: remove after applied for the first time.
  file { "${hooks_directory}/pre-receive":
    ensure => absent,
  }

  file { "${hooks_directory}/post-receive":
    content => template('tails/gitolite/hooks/tails-post-receive.erb'),
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package[curl],
  }

  # TODO: remove after deploy
  file { "${hooks_directory}/update.secondary":
    ensure => absent,
  }

  # TODO: remove after deploy
  file { "${hooks_directory}/langs.json":
    ensure =>  absent,
  }

  ensure_packages(['curl'])

}
