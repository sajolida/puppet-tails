# Set up a cronjob for a meeting

define tails::meeting::reminder (
  Tuple[Pattern[/.+@.+/], 1, default] $addresses,
  String $subject,
  String $template,
  Integer $day_of_month,
  Enum['present', 'absent'] $ensure     = 'present',
  Tuple[Integer, 1, default] $reminders = [10],
  Pattern[/.+@.+/] $fromemailaddress    = 'noreply@tails.boum.org',
  $cron_hour                            = 8,
  $cron_minute                          = 15,
) {

  #
  # Sanity checks
  #

  validate_email_address($fromemailaddress)
  $addresses.each |String $address| {
    validate_email_address($address)
  }

  #
  # Resources
  #

  $reminders_str = join($reminders, ',')
  $addresses_str = join($addresses, ',')

  file { "${tails::meeting::homedir}/${template}":
    ensure => $ensure,
    owner  => $tails::meeting::user,
    group  => 'root',
    mode   => '0600',
    source => "puppet:///modules/tails/meeting/${template}"
  }

  cron { "meeting-reminder-${name}":
    ensure  => $ensure,
    command => "'${tails::meeting::script_path}' --reminder '${reminders_str}' --addresses '${addresses_str}' --day '${day_of_month}' --from '${fromemailaddress}' --subject '${subject}' --template '${tails::meeting::homedir}/${template}'", # lint:ignore:140chars -- command
    user    => $tails::meeting::user,
    hour    => $cron_hour,
    minute  => $cron_minute,
    require => [
      Class['::tails::meeting'],
      File["${tails::meeting::homedir}/${template}"],
    ],
  }
}
