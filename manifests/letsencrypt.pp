# Wrapper around the letsencrypt class
class tails::letsencrypt (
  String $email = 'tails-sysadmins@boum.org',
  Hash $config  = {
    'agree-tos'           => 'True',
    'authenticator'       => 'webroot',
    'keep-until-expiring' => 'True',
    'post-hook'           => '/bin/systemctl reload nginx.service',
    'rsa-key-size'        => 4096,
    'server'              => 'https://acme-v02.api.letsencrypt.org/directory',
    'webroot-path'        => '/var/www/html',
  },
) {

  # Sanity checks

  validate_email_address($email)

  # Packages

  ensure_packages([gnutls-bin]) # ships certtool

  package { 'certbot':
    ensure  => installed,
  }

  # Other resources

  class { '::letsencrypt':
    email           => $email,
    manage_install  => false,
    install_method  => 'package',
    package_command => 'certbot',
    config          => $config,
    require         => Package[certbot],
  }

}
