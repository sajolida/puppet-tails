#!/bin/sh

# This script is called by the compare_artifacts jenkins-job-builder
# builder, defined in jenkins-jobs.git.

set -e
set -u
set -x

[ "${WORKSPACE}" ] || exit 3

[ -d "${WORKSPACE}" ] || exit 4

ARTIFACTS_DIR="${WORKSPACE}/build-artifacts"

[ -d "${ARTIFACTS_DIR}" ] || exit 5
[ -d "${ARTIFACTS_DIR}"/1 ] || exit 6
[ -d "${ARTIFACTS_DIR}"/2 ] || exit 7

exit_code=0

dothediffo()
{
    EXT="$1"
    echo "Running diffoscope for ${EXT} files on ${GIT_BRANCH}"
    IMAGEFILES=$(echo "${ARTIFACTS_DIR}"/1/tails-*."${EXT}" "${ARTIFACTS_DIR}"/2/tails-*."${EXT}")
    if sudo TMPDIR="${WORKSPACE}" diffoscope \
                --text     "${ARTIFACTS_DIR}/diffoscope.${EXT}.txt" \
                --html     "${ARTIFACTS_DIR}/diffoscope.${EXT}.html" \
                --max-report-size 262144000 \
                --max-diff-block-lines 10000 \
                --max-diff-input-lines 10000000 \
                --profile=- \
                $IMAGEFILES; then
        sudo rm $IMAGEFILES
    else
        exit_code=$((exit_code + $?))
        sudo chown -R jenkins:jenkins "${ARTIFACTS_DIR}"
        if [ -r "${ARTIFACTS_DIR}/diffoscope.${EXT}.txt" ] \
           && [ $((3 * 1024)) -ge "$(stat --format='%s' "${ARTIFACTS_DIR}/diffoscope.${EXT}.txt")" ]; then
            cat "${ARTIFACTS_DIR}/diffoscope.${EXT}.txt"
        fi
    fi
}

dothediffo "iso"
dothediffo "img"

exit $exit_code
