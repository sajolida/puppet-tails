#!/usr/bin/python3
# Copyright (c) 2018 Muri Nicanor <muri@immerda.ch>. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#  3. Neither the name of the copyright holder nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import datetime
from datetime import timedelta
import argparse
import smtplib
import socket
from string import Template
from email.mime.text import MIMEText
from email.utils import parseaddr
from pathlib import Path
import sys


# calculate the date of the next meeting
# the default is the 3rd of the next month
def nextMeeting(mon=None, meetingday=None):
    mon = mon or 0
    meetingday = meetingday or 3
    todayDate = datetime.date.today()

    # if no specific month was given, check if the date
    # is in this months future- if not, take the next
    # month
    if mon == 0:
        if (todayDate.day >= meetingday):
            if todayDate.month == 11:
                mon = 12
            else:
                mon = (todayDate.month + 1) % 12
        else:
            mon = todayDate.month

    # if the month is in the past, calculate for the month next year
    if mon < todayDate.month:
        todayDate = todayDate.replace(year=todayDate.year + 1)

    # calulate the meetingdate
    try:
        nextMeetingDate = todayDate.replace(month=mon, day=meetingday)
    except ValueError as e:
        sys.exit("The month/day combination month: {}, "
                 "day: {} is invalid: {}".format(mon, meetingday, e))

    # if the meetingday falls on a Friday, Saturday, or Sunday,
    # then choose the day three days after
    if nextMeetingDate.weekday() >= 4:
        nextMeetingDate += timedelta(days=3)
    return nextMeetingDate


# create a mailbody from the template
def mailbody(date):
    try:
        with open(template) as t:
            src = Template(t.read())
    except PermissionError as e:
        sys.exit("Could not open {}: {}".format(template, e))
    d = {'date': date.strftime("%A %d. %B %Y")}
    return src.substitute(d)


# send a mail using sendmail
def sendmail(fromaddress, subject, date, address):
    message = mailbody(date)
    msg = MIMEText(message)
    humanreadabledate = date.strftime("%A %B %d")
    msg['Subject'] = "{}{}".format(subject, humanreadabledate)
    msg['From'] = fromaddress
    msg['To'] = address
    try:
        s = smtplib.SMTP('localhost')
        s.sendmail(msg['From'], msg['To'], msg.as_string())
        s.quit()
    except socket.error as e:
        print("Could not connect to mailserver on localhost. "
              "Mail wasn't sent to {}: {}".format(address, e))


# the main function parses and validates the arguments and
# then either prints the info to stdout or calls other
# methods to generate a mail body or send mails
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--month", type=int,
                        help="Set the month (default is next month).",
                        choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    parser.add_argument("-r", "--reminders", type=str,
                        help="Set which days before a meeting should be a "
                        "'reminder day'.")
    parser.add_argument("-d", "--day", type=int,
                        help="Set the day the meeting should happen.")
    parser.add_argument("-t", "--template", type=str,
                        help="Pass a template for the email.",
                        default="FT_meeting_template.eml")

    parser.add_argument("-a", "--addresses", type=str,
                        help="List of addresses a mail should be "
                        "sent to (seperated by comma)")
    parser.add_argument("-f", "--from", type=str,
                        help="From address for sending mails.",
                        default="noreply@tails.boum.org")
    parser.add_argument("-s", "--subject", type=str,
                        help="Subject of mail (date will be appended)",
                        default="Tails foundations team meeting: ")

    parser.add_argument("-p", "--print", action="store_true",
                        help="Print mail body on stdout")

    args = parser.parse_args()

    fromaddress = getattr(args, 'from')
    subject = args.subject
    template = args.template
    if not Path(template).is_file():
        sys.exit("{} is not a file".format(template))

    # make a list of the addresses given in --to
    # for every address we test if it is rfc-822 conform
    # and if it contains an '@'
    if args.addresses:
        toaddresses = [address for address in args.addresses.split(',')]
        for address in toaddresses:
            if parseaddr(address) == ('', '') or '@' not in address:
                sys.exit("{} is not a valid email address.".format(address))

    # calculate the next meeting date
    date = nextMeeting(args.month, args.day)

    # make a list of dates that should be reminderdates
    reminders = []
    if args.reminders:
        for item in [int(item) for item in args.reminders.split(',')]:
            reminders.append(date - timedelta(days=item))

    # is today a reminderday?
    remindertoday = date.today() in reminders

    # print the date of the next meeting(s) to stdout
    # if one or more reminder dates are set, it also lists those
    if not args.print and not args.addresses:
        print("The monthly Tails meeting in {} will be on {}".format(
            date.strftime("%B"),
            date.strftime("%A %d. %B %Y")))
        for reminder in sorted(reminders):
            tail = " - thats today!" if date.today() == reminder else "."
            msg = "One reminder would be sent on {}{}".format(
                    reminder.strftime("%A %d. %B %Y"), tail)
            print(msg)
    # if the current date is a reminderday or no reminderdays are specified,
    # either print a mail to stdout or send a mail using sendmail
    if remindertoday or not reminders:
        if args.print:
            print(mailbody(date))
        elif args.addresses:
            for address in toaddresses:
                sendmail(fromaddress, subject, date, address)
