#!/usr/bin/python3

import argparse
import os.path

from bsddb3 import db

parser = argparse.ArgumentParser()
parser.add_argument('--path', required=True,
                    help="Path to the file that contains the database")
parser.add_argument('--db', required=True,
                    help="Name of the database (for example: references)")
parser.add_argument('--compact-fillpercent', default=90, type=int,
                    help="Any page in the database below this percentage full is considered for compaction")
args = parser.parse_args()

size_before = os.path.getsize(args.path)
print("Before compacting: {size} bytes".format(size=size_before))

d = db.DB()
d.set_cachesize(0, 536870912) # 512 MB
d.open(args.path, dbname=args.db)
for i in range(0, 4):
    d.compact(compact_fillpercent=args.compact_fillpercent,
              flags=db.DB_FREE_SPACE)
    print("After pass {n}: {size} bytes"
          .format(n=i, size=os.path.getsize(args.path)))
d.close()

size_after = os.path.getsize(args.path)
print("After closing the database: {size} bytes".format(size=size_after))
print("⇒ saved {size} bytes ({percent}%)"
      .format(size=size_before - size_after,
              percent=int(100*(size_before - size_after)/size_before)
      ))
