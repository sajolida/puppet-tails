Tails module for Puppet

Dependencies
============

Many classes depend on the puppetlabs-stdlib module.

Many classes depend on the saz/sudo module.

tails::reprepro depends on the reprepro module:
https://gitlab.tails.boum.org/tails/puppet-reprepro

The tails and tails::reprepro classes depend on the shared-common module:
https://gitlab.com/shared-puppet-modules-group/common

The tails::apt class depends on:

 - https://github.com/puppetlabs/puppetlabs-apt
 - https://gitlab.com/baldurmen/puppet-apt_listchanges

The tails::check_mirrors class depends on the vcsrepo module:
https://github.com/puppetlabs/puppetlabs-vcsrepo

The tails::gitolite class depends on the gitolite module:
https://gitlab.tails.boum.org/tails/puppet-gitolite

The tails::jenkins class depends on the jenkins module:
https://gitlab.tails.boum.org/tails/puppet-jenkins

Classes
=======

tails::base
-----------

This class is included on every team-admin'd Tails system.

tails::iso_builder
------------------

This class manages a system able to build Tails.

tails::check_mirrors
--------------------

This class manages periodic checks of Tails mirrors.

tails::gitolite
---------------

This class manages the Tails gitolite setup that is used by the
puppetmaster and other services.

tails::jenkins::master
----------------------

This class manages Tails' Jenkins master setup.

It includes the jenkins class itself.

tails::reprepro
---------------

This class manages Tails' reprepro setup.

It includes the reprepro class itself.

tails::tester
-------------

This class manages a system able to run the Tails automated test suite.

tails::whisperback::relay
-------------------------

This class manages a Tails WhisperBack SMTP relay.

License and copyright
=====================

Unless explicitly noted in individual files, this module is:

Copyright (C) 2012-2019 Tails developers <tails@boum.org>
GPLv3
