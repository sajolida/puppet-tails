Update Puppet dependencies files
================================

We choose to install packages either from Debian packages or directly from
upstream repositories, because we don't have a way to verify packages
downloaded from Pypi.

The script in this directory extracts direct requirements of Weblate from its
repository, queries Pypi for package information, traverses Weblate's full
dependency tree, and generates Puppet code for declaring all dependencies.

Usage
-----

usage: update_puppet_dependency_files.py [-h] --weblate-version WEBLATE_VERSION
                                         --debian-suite
                                         {stretch,buster,bullseye}
                                         --weblate-repo WEBLATE_REPO
                                         --config-file CONFIG_FILE

optional arguments:
  -h, --help            show this help message and exit
  --weblate-version WEBLATE_VERSION
                        What Weblate version.
  --debian-suite {stretch,buster,bullseye}
                        Debian suite.
  --weblate-repo WEBLATE_REPO
                        Path to Weblate repository.
  --config-file CONFIG_FILE
                        Path to configuration file.

Dependencies
------------

Type `make` to install dependencies using sudo (for Python packages in
the Debian archive) and a virtualenv (for the rest).


Configuration
-------------

It accepts a YAML configuration file containing a dict with the following
entries:

    'skip': A list of packages to be skipped.

    'require_pip': A list of packages that must be installed from pip if they
                   appear somewhere in the dependency tree.

    'extra_pip': A list of extra packages to be installed from pip regardless
                 of being dependencies of other packages.

    'require_debian': A list of packages that must be installed from Debian.

    'pip': A dictionary indexed by pip package names in which each item is
           itself a dict that may contain a 'url', a 'repo_type', and a list
           of additional conditions for the package version to be installed.

Example
-------

  python3 ./update_puppet_dependecy_files.py \
    --weblate-version 3.5.1 \
    --debian-suite stretch \
    --weblate-repo ~/weblate/
